import eli5
import pandas as pd

income = pd.read_csv("income.csv")
income.head()


class CategoricalTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, new_features=True):
        self.new_features = new_features

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        df = X.copy()
        if self.new_features:
            df["workclass"] = df["workclass"].replace("?", "Unknown")
            df.loc[df["native_country"] != " United-States",
                   "native_country"] = "non-usa"

        for name in df.columns.to_list():
            col = pd.Categorical(df[name])
            df[name] = col.codes

        return df


categorical_features = income.select_dtypes("object").columns.to_list()

numerical_features = income.select_dtypes("int64").columns.to_list()

categorical_pipeline = Pipeline(steps=[("cat_selector", FeatureSelector(categorical_reatures)),
                                       ("cat_transformer", CategoricalTransformer())
                                       ]
                                )

numerical_pipeline = Pipeline(steps=[("num_selector", FeatureSelector(numerical_features)),
                                     ("stf_scaler", MinMaxScaler())
                                     ]
                              )

full_pipeline_preprocessing = FeatureUnion(transformer_list=[("categorical_pipeline", categorical_pipeline),
                                                             ("numerical_pipeline",
                                                              numerical_pipeline)
                                                             ]
                                           )

pipe = Pipeline(steps=[("full_pipeline", full_pipeline_preprocessing),
                       ("fs", SelectKBest()),
                       ("clf", RandomForestClassifier())
                       ]
                )

search_space = [
    {
        "clf": [DecisionTreeClassifier()],
        "clf__criterion": ["gini", "entropy"],
        "clf__splitter": ["best", "random"],
        "clf__random_state": [seed],
        "fs__score_func": [chi2],
        "fs__k": [4, 6, 8],
    },
    {
        "clf": [DecisionTreeClassifier()],
        "clf__n_estimators": [300, 400],
        "clf__criterion": ["gini", "entropy"],
        "clf__max_leaf_noes": [64, 128, 256],
        "clf__random_state": [seed],
        "clf__bootstrap": [True, False],
        "fs__score_func": [chi2],
        "fs__k": [4, 6, 8],
    },
    {
        "clf": [BaggingClassifier()],
        "clf__base_estimator": [DecisionTreeClassifier(criterion="entropy", splitter="random")],
        "clf__bootstrap": [True, False]
    }
]

kfold = KFold(n_splits=num_folds, random_state=seed)

grid = GridSearchCV(estimator=pipe,
                    param_grid=search_space,
                    cv=kfold,
                    scoring=scoring,
                    return_train_score=True,
                    n_jobs=-1,
                    refit="AUX",
                    verbose=10
                    )

best_model = grid.fit(X_train, y_train)

print(result[result.rank_test_AUC == 1][["mean_train_AUC",
                                         "std_train_AUC", "mean_test_AUC", "std_test_AUC"]])

clf = RandomForestClassifier(bootstrap=True, class_weight=None, criterion="entropy",
                             max_depth=None, max_features="auto", max_leaf_nodes=256,
                             min_impurity_decrease=0.0, min_impurity_split=None, min_samples_leaf=1,
                             min_samples_split=2, min_weight_fraction_leaf=0.0, n_estimators=400,
                             n_jobs=None, oob_score=True, random_state=42, verbose=0, warm_start=False)

eli5.show_prediction(clf, X_test.iloc[2],
                     feature_names=list(new_data_df.columns),
                     show_featre_values=True)

eli5.show_prediction(clf, X_test.iloc[0],
                     feature_names=list(new_data_df.columns),
                     show_feature_values=True)

predict = clf.predict(X_test)
print(accuracy_score(y_test, predict))
print(confusion_matrix(y_test, predict))
print(classification_report(y_test, predict))
