import keras
from keras.layers import SimpleRNN as RNN
import os
import fnmatch
from scipy.io import wavfile
import scipy
import sklearn.preprocessing as preprocessing
from matplotlib import pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, SimpleRNN
import numpy as np
import scipy.fftpack as fft
from sklearn.metrics import mean_squared_error


def recursive_files():
    matches = []
    for root, dirnames, filenames in os.walk('./'):
        for filename in fnmatch.filter(filenames, '*.wav'):
            matches.append(os.path.join(root, filename))
    return matches


files = recursive_files()
files = sorted(files)
files

all_waves = []

for file in files:
    name = file.split('/')[-1]
    fs, signal = wavfile.read(name)
    secs = signal.shape[0] / float(fs)
    Ts = 1.0/fs
    t = scipy.arange(0, secs, Ts)
    try:
        if("predict" in name):
            predict = list(signal)
        if("compare" in name):
            compare = list(signal)
        insert = [name, list(signal[0:2205])]
        all_waves.append(insert)
    except IndexError:
        print(IndexError)
        continue

hX = []
hy = []
X = []
y = []

for line in all_waves:
    if("pure" in line[0]):
        hX.append(line[0])
        for steps in range(len(line[1])-9):
            d = steps + 10
            X.append(line[1][steps:d])
    elif("dist" in line[0]):
        hy.append(line[0])
        y.append(line[1])
    else:
        y_test = line[1]

print(hX)
print(hy)

predict_treated = []
for i in range(len(predict)-9):
    d = i + 10
    predict_treated.append(predict[i:d])

X_norm = preprocessing.maxabs_scale(X)
y_norm = preprocessing.maxabs_scale(y)

predict_norm = preprocessing.maxabs_scale(predict_treated)
compare_norm = preprocessing.maxabs_scale(compare[0:-9])

plot1 = plt.plot(scipy.arange(0, (1/44100*2205), 1/44100),
                 all_waves[10][1], "g")
plot2 = plt.plot(scipy.arange(0, (1/44100*2205), 1/44100),
                 all_waves[11][1], "r")
plt.show()

X_norm_array = np.array(X_norm)
X_norm_shaped = np.reshape(
    X_norm_array, (X_norm_array.shape[0], 1, X_norm_array.shape[1]))

predict_norm_array = np.array(predict_norm)
predict_norm_shaped = np.reshape(
    predict_norm_array, (predict_norm_array.shape[0], 1, predict_norm_array.shape[1]))

print("Tamanho dados *entrada*:", X_norm_shaped.shape)
print("Tamanho dados *predict*:", predict_norm_shaped.shape)

Y = []
for i in y_norm:
    for j in range(len(i)-9):
        Y.append(i[j])

Y_array = np.array(Y)
print("Tamanho dados *saída*: ", Y_array.shape)
print("Tamanho dados *compare*: ", len(compare_norm))

model = Sequential()
# Units - pesquisar
# Input shape é que o que vai entrar é uma lista de um item com dez itens dentro
# Relu é um algoritmo de ativação
model.add(SimpleRNN(units=32, input_shape=(1, 10), activation="relu"))
model.add(Dense(8, activation="relu"))
model.add(Dense(1))
model.compile(loss="mean_squared_error", optimizer="rmsprop")
print(model.summary())

print(model.fit(X_norm_shaped, Y_array, epochs=100, batch_size=16, verbose=2))

predicted = model.predict(predict_norm_shaped)

trainScore = model.evaluate(predict_norm_shaped, compare_norm, verbose=0)
print(trainScore)

predicted = predicted[:, 0]
plt.plot(predicted[0:2205], c='r')
plt.plot(compare_norm[0:2205], c='b')
plt.show()
print(predicted)

FFT = abs(scipy.fft(predicted[0:44100]))
FFT = preprocessing.maxabs_scale(FFT)
FFT_side = FFT[:1000]
freqs = fft.fftfreq(predicted[0:44100].size, 1/44100)
fft_freqs = np.array(freqs)
freqs_side = freqs[:1000]
fft_freqs_side = np.array(freqs_side)

FFT1 = abs(scipy.fft(compare_norm[0:44100]))
FFT1 = preprocessing.maxabs_scale(FFT1)
FFT_side1 = FFT1[:1000]
freqs1 = fft.fftfreq(compare_norm[0:44100].size, 1/44100)
fft_freqs1 = np.array(freqs1)
freqs_side1 = freqs1[:1000]
fft_freqs_side1 = np.array(freqs_side1)

# FFT2 = abs(scipy.fft(predict_array[0:44100]))
# FFT2 = preprocessing.maxabs_scale(FFT2)
# FFT_side2 = FFT2[:1000]
# freqs2 = fft.fftfreq(predict_array[0:44100].size, 1/44100)
# fft_freqs2 = np.array(freqs2)
# freqs_side2 = freqs2[:1000]
# fft_freqs_side2 = np.array(freqs_side2)

print(mean_squared_error(FFT1, FFT))

print("Com distorção (predito pelo modelo)")
plt.subplot(313)
p3 = plt.plot(freqs_side, abs(FFT_side), "r")
plt.xlabel('Frequency (Hz)')
plt.ylabel('Count single-sided')
plt.show()

print("Com distorção (previsto)")
plt.subplot(313)
p4 = plt.plot(freqs_side1, abs(FFT_side1), "b")
plt.xlabel('Frequency (Hz)')
plt.ylabel('Count single-sided')
plt.show()

# print("Sem distorção")
# plt.subplot(313)
# p4 = plt.plot(freqs_side2, abs(FFT_side2), "y")
# plt.xlabel('Frequency (Hz)')
# plt.ylabel('Count single-sided')
# plt.show()

wavfile.write("./result-cleanchord-a.wav", 44100, predicted)
